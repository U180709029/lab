public class FindMin {
	public static void main(String[] args){
		int value1 = Integer.parseInt(args[0]);	
		int value2 = Integer.parseInt(args[1]);	
		int value3 = Integer.parseInt(args[2]);	
		int min = 0;
		int temp = 0;

		if (value1 < value2){
			temp = value1;
		}else {
			temp = value2;
		}

		if (temp < value3){
			min = temp;
		}else{
			min = value3;		
		}

		System.out.println(min);
	}
}
