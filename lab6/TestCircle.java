public class TestCircle {

    public static void main(String[] args) {

        Circle circle = new Circle(4,new Point(6,8));
        Circle circle2 = new Circle(5, new Point(0,0));
        System.out.println(circle.intersect(circle2));
        System.out.println("area = " + circle.area());

    }
}
